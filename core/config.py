import os
from dotenv import load_dotenv
from pathlib import Path
env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)


class Settings:
    PROJECT_NAME:str = "API PRUEBA"
    PROJECT_VERSION:str = "1.0"
    DB_USER:str = os.getenv('DB_USER')
    DB_PASS:str = os.getenv('DB_PASS')
    DB_NAME:str = os.getenv('DB_NAME')
    DB_PORT:str = os.getenv('DB_PORT')
    SERVER:str = os.getenv('SERVER')
    DB_URL:str = f"mysql+pymysql://{DB_USER}:{DB_PASS}@{SERVER}:{DB_PORT}/{DB_NAME}"

setting = Settings()