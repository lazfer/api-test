
def test_getAll(app, client):
    with app.app_context():
        response = client.get('/products')
        assert response.status_code == 200
        