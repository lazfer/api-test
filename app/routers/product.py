from fastapi import APIRouter, HTTPException, Depends
from app.schemas import Product, UpdateProduct, BuyProduct
from app.db.database import get_db
from sqlalchemy.orm import Session
from notifypy import Notify
from app.querys import product
import schedule
import time
import datetime

# Router principal
router = APIRouter(
    prefix="/api",
    tags=["Products"]
)
      
# Inicio de nuestra api
# response_model=List[ShowProduct]
@router.get('/')
def read_root():
    return { "API TEST" }

# Obtiene los producto
@router.get('/products')
def get_products(db:Session = Depends(get_db)):
    products = product.getProducts(db)
    return products

# Obtiene un producto
# @router.get('/product/{product_sku}',response_model=ShowProduct)
@router.get('/product/{product_sku}')
def get_product(product_sku: str, db:Session = Depends(get_db)):
    resProduct = product.getProduct(product_sku, db)
    return resProduct
    #raise HTTPException(status_code=404, detasil="Producto no encontrado")


# Guarda producto y valida que sku no sea duplicado
@router.post('/products')
def save_product(productSave: Product, db:Session = Depends(get_db)):
    product.createProduct(productSave, db)
    return {"message":"Producto creado correctamente"}


# Borra producto
@router.delete('/product/{product_sku}')
def del_product(product_sku: str, db:Session = Depends(get_db)):
    resDelProduct = product.deleteProduct(product_sku, db)
    return resDelProduct
  
    
    
# Actualizar stock
@router.patch('/inventories/product/{product_id}')
def update_product(product_id: str, update_product: UpdateProduct, db:Session = Depends(get_db)):
    print("update_product",update_product)
    patchProduct = product.patchProduct(product_id, update_product, db)
    return patchProduct

# Comprar articulo
@router.post('/orders')
def save_product(buy: BuyProduct, db:Session = Depends(get_db)):
    productList = dict(buy)
    resProduct = product.getProduct(productList["sku"],db)
    try:
        if "detail" in resProduct:
            return resProduct
    except:
        subtractStock = resProduct.stock - productList["amount"]
        if subtractStock < 10:
            hours_now = datetime.datetime.now()
            hours_add = hours_now + datetime.timedelta(seconds=2)
            schedule.every().day.at(hours_add.strftime('%H:%M:%S')).do(job)  
            i = 0
            while i <= 3:
                i+=1
                schedule.run_pending()
                time.sleep(1)
    return {"message": "Producto comprado"}
def job():
    notify = Notify()
    notify.title = "Producto agotado"
    notify.message = "Su producto se termino"
    notify.send()
    print("Se ejecuto job")


