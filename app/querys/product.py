
from sqlalchemy.orm import Session
from fastapi import HTTPException
from app.db import models

def createProduct(product, db:Session):
    productList = dict(product)
    print('list', productList)
    new_product = models.Product(
    sku = productList["sku"],
    name = productList["name"],
    stock = 100
    )
    db.add(new_product)
    db.commit()
    db.refresh(new_product)
    
def getProducts(db:Session):
    data = db.query(models.Product).all()
    return data
        
def getProduct(product_sku, db:Session):
    product = db.query(models.Product).filter(models.Product.sku == product_sku).first()
    if not product:
        raise HTTPException(status_code=404, detail="Producto no encontrado")
    return product


def deleteProduct(product_sku, db:Session):
    product = db.query(models.Product).filter(models.Product.sku == product_sku)
    if not product.first():
         raise HTTPException(status_code=404, detail="Producto no encontrado")
    product.delete(synchronize_session=False)
    db.commit()
    return {"message": "Producto eliminado correctamente"}

def patchProduct(product_id,update_product,db:Session):
    product = db.query(models.Product).filter(models.Product.sku == product_id)
    if not product.first():
         raise HTTPException(status_code=404, detail="Producto no encontrado")
    product.update(update_product.model_dump(exclude_unset=True))
    db.commit()
    return {"message":"Se actualizo el stock del producto"}
    


