from pydantic import BaseModel
from datetime import datetime
from typing import Optional
# Modelo de datos
class Product(BaseModel):
    sku: str
    name: str
    stock: int = None
    create_at: datetime = datetime.now() 
    
class ShowProduct(BaseModel):
    sku:str
    name:str
    stock:int
    class Config():
        orm_mode = True
        
## stock:int = None 
class UpdateProduct(BaseModel):
   stock:int
   
class BuyProduct(BaseModel):
    sku: str
    amount: int