from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from core.config import setting

DB_USER = 'root'
DB_PASS = ''
DB_NAME = 'products'
DB_PORT = '3306'
    
DB_URL = setting.DB_URL
engine = create_engine(DB_URL)
sessionLocal = sessionmaker(bind=engine, autocommit=False,autoflush=False)
Base = declarative_base() 


def get_db():
    db = sessionLocal()
    try:
        yield db
    finally:
        db.close()