from app.db.database import Base
from sqlalchemy import Column,Integer,String,DateTime
from datetime import datetime

# Creacion de modelo
class Product(Base):
    __tablename__ = "products"
    id = Column(Integer,primary_key=True,autoincrement=True)
    sku = Column(String(50),unique=True)
    name = Column(String(100))
    stock = Column(Integer)
    create_at = Column(DateTime,default=datetime.now,onupdate=datetime.now)

